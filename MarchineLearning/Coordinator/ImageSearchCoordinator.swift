//
//  ImageSearchCoordinator.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 9/4/21.
//

import UIKit


class ImageSearchCoordinator: Coordinator {
    let presenter: UINavigationController
    let imageSearchDataManager: ImageSearchDataManager
    var imageSeachViewModel: ImageSearchViewModel?

    init(presenter: UINavigationController, imageSearchDataManager: ImageSearchDataManager) {
        self.presenter = presenter
        self.imageSearchDataManager = imageSearchDataManager
    }
    
    
    override func start() {
        let imageSearchViewModel = ImageSearchViewModel(imageSearchDataManager: imageSearchDataManager)
        let imageSearchViewController = ImageSearchViewController(viewModel: imageSearchViewModel)
        imageSearchViewController.title = NSLocalizedString("Buscador", comment: "")
        imageSearchViewModel.viewDelegate = imageSearchViewController
        imageSearchViewModel.coordinatorDelegate = self
        self.imageSeachViewModel = imageSearchViewModel
        presenter.pushViewController(imageSearchViewController, animated: true)
    }
    
    override func finish() {}
}


extension ImageSearchCoordinator: ImageSearchCoordinatorDelegate {
    func didSelect(image: Image) {
        let imageDetailViewModel = ImageDetailViewModel(image: image)
        imageDetailViewModel.coordinatorDelegate = self
        let imageDetailViewController = ImageDetailViewController(viewModel: imageDetailViewModel)
        imageDetailViewController.title = NSLocalizedString("Detalle", comment: "")
        imageDetailViewModel.viewDelegate = imageDetailViewController
        presenter.pushViewController(imageDetailViewController, animated: true)
    }
    
}


extension ImageSearchCoordinator: ImageDetailCoordinatorDelegate {
    func imageDetailCancelButtonTapped() {
        presenter.popViewController(animated: true)
    }
    
    
}
