//
//  VisionClassifier.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 9/4/21.
//
import UIKit
import Vision



class VisionClassifier {
    
    private let model: VNCoreMLModel
    private var completion: (String) -> Void = { _  in }
    
    private lazy var requests: [VNCoreMLRequest] = {
        
        let request = VNCoreMLRequest(model: model) { (request, error) in
        
            
            guard let results = request.results as? [VNClassificationObservation] else {
                return
            }
            
            if !results.isEmpty {
                if let result = results.first {
                    self.completion(result.identifier.lowercased())
                }
            }
            
        }
        
        request.imageCropAndScaleOption = .centerCrop
        return [request]
        
    }()
    
    init?(coreModel: VNCoreMLModel) {
        self.model = coreModel
    }
    
    func classify(_ image: UIImage, completion: @escaping (String) -> Void) {
        
        self.completion = completion
        
        DispatchQueue.global().async {
            
            guard let cgImage = image.cgImage else {
                return
            }
            
            
            let handler = VNImageRequestHandler(cgImage: cgImage, options: [:])
            
            do {
                try handler.perform(self.requests)
            } catch {
                print(error.localizedDescription)
            }
            
        }
        
    }
    
}
