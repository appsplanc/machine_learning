//
//  ImageDetailCoordinator.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 6/4/21.
//

import UIKit

class ImageDetailCoordinator: Coordinator {
    let presenter: UINavigationController
    var imageDetailNavigationController: UINavigationController?
    var onCancelTapped: (() -> Void)?

    init(presenter: UINavigationController) {
        self.presenter = presenter
    }
    
    override func start() {
        let iamgeDetailViewModel = ImageDetailViewModel()
        iamgeDetailViewModel.coordinatorDelegate = self

        let addTopicViewController = ImageDetailViewController(viewModel: iamgeDetailViewModel)
        iamgeDetailViewModel.viewDelegate = addTopicViewController
        addTopicViewController.isModalInPresentation = true
        addTopicViewController.title = "Detail image"

        let navigationController = UINavigationController(rootViewController: addTopicViewController)
        self.imageDetailNavigationController = navigationController
        presenter.present(navigationController, animated: true, completion: nil)    }
    
    override func finish() {
        imageDetailNavigationController?.dismiss(animated: true, completion: nil)
    }
}

extension ImageDetailCoordinator: ImageDetailCoordinatorDelegate {
    func imageDetailCancelButtonTapped() {
        onCancelTapped?()
    }
}
