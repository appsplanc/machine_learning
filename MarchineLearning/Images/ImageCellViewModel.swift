//
//  ImageCellViewModel.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 5/4/21.
//

import UIKit

class ImageCellViewModel {
    let image: Image
    
    init(image: Image) {
        self.image = image
    }
}
