//
//  ImageSearchCell.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 9/4/21.
//

import UIKit

class ImageSearchCell: UICollectionViewCell {
    
    
    
    @IBOutlet weak var imageView: UIImageView!
 
    
    var viewModel: ImageSearchCellViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }
            imageView.image =  viewModel.image.imageUI
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        imageView.layer.cornerRadius = imageView.frame.size.width / 2
        imageView.clipsToBounds = true
    }
}
