//
//  ImageSearchViewModel.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 9/4/21.
//

import UIKit
import Vision

protocol ImageSearchCoordinatorDelegate: class {
    func didSelect(image: Image)
}

protocol ImageSearchViewDelegate: class {
    func imageFetched()
    func showProgressView()
    func hideProgressView()
}

class ImageSearchViewModel {

    private var classifier: VisionClassifier?
    weak var coordinatorDelegate: ImageSearchCoordinatorDelegate?
    weak var viewDelegate: ImageSearchViewDelegate?
    let imageSearchDataManager: ImageSearchDataManager
    var initImageSearch: [ImageSearchCellViewModel] = []
    var imageSearchCellViewModel: [ImageSearchCellViewModel] = []
    var indice: Int = 0
    
    init(imageSearchDataManager: ImageSearchDataManager) {
        self.imageSearchDataManager = imageSearchDataManager
        do {
            let coreModel = try VNCoreMLModel(for: ImageClassifier(configuration: MLModelConfiguration()).model)
            self.classifier = VisionClassifier(coreModel: coreModel)
        } catch {
            fatalError("ML model: \(error)")
        }
    }
    
    func viewWasLoaded() {
        imageSearchDataManager.fetchSearchAllImage { images in
            guard let images = images else {return}
            self.initImageSearch = images.map({ ImageSearchCellViewModel(image: $0 ) })
        }
    }
    
    func searchLoaded(search: String) {
        self.imageSearchCellViewModel = []
        self.viewDelegate?.imageFetched()
        self.indice = 0
        self.viewDelegate?.showProgressView()
        classifier(search: search)
    }
    
    
    private func classifier(search: String) {
        if self.indice < initImageSearch.count {
            guard let image = initImageSearch[indice].image.imageUI else {
                self.indice = self.indice + 1
                self.classifier(search: search)
                return
            }
            self.classifier?.classify(image) { [weak self] result in
                guard let self = self else { return }
                if (result == search.lowercased()) {
                    self.imageSearchCellViewModel.append(ImageSearchCellViewModel(image: Image(imageUI: image)))
                    DispatchQueue.main.async {
                        self.viewDelegate?.imageFetched()
                    }
                }
                self.indice = self.indice + 1
                self.classifier(search: search)
                
            }
        } else {
            DispatchQueue.main.async {
                self.viewDelegate?.hideProgressView()
            }
        }
    }
    
    func numberOfSections() -> Int {
        return 1
    }

    func numberOfRows(in section: Int) -> Int {
        return imageSearchCellViewModel.count
    }

    func viewModel(at indexPath: IndexPath) -> ImageSearchCellViewModel? {
        guard indexPath.row < imageSearchCellViewModel.count else { return nil }
        return imageSearchCellViewModel[indexPath.row]
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        coordinatorDelegate?.didSelect(image: self.imageSearchCellViewModel[indexPath.row].image)
    }
}
