//
//  Utils.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 10/4/21.
//

import UIKit
import CoreML
import Vision

class Util {
    static func drawDetectionsOnPreview(image: UIImage?, detections: [VNRecognizedObjectObservation]) -> UIImage? {
        guard let image = image else { return nil }
        
        let imageSize = image.size
        let scale: CGFloat = 0
        UIGraphicsBeginImageContextWithOptions(imageSize, false, scale)

        image.draw(at: CGPoint.zero)
        
        let detectionsFilter = detections.filter({$0.confidence > 0.02})

        for detection in detectionsFilter {
            
            let boundingBox = detection.boundingBox
            let rectangle = CGRect(x: boundingBox.minX*image.size.width,
                                   y: (1-boundingBox.minY-boundingBox.height)*image.size.height,
                                   width: boundingBox.width*image.size.width,
                                   height: boundingBox.height*image.size.height)
            
            UIColor(red: 0, green: 1, blue: 0, alpha: 0.4).setFill()
            UIRectFillUsingBlendMode(rectangle, CGBlendMode.normal)
            
            
            let textColor = UIColor.white
            let textFont = UIFont(name: "Helvetica", size: self.ip(percent: 12, rectangle: rectangle))!

            let textFontAttributes = [
                NSAttributedString.Key.font: textFont,
                NSAttributedString.Key.foregroundColor: textColor,
                ] as [NSAttributedString.Key : Any]

            let rect = CGRect(x: boundingBox.minX*image.size.width,
                              y: (1-boundingBox.minY-boundingBox.height)*image.size.height,
                              width: rectangle.width,
                              height: rectangle.height)
            
            detection.labels[0].identifier.draw(in: rect, withAttributes: textFontAttributes)
        }
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    static private func ip(percent: CGFloat, rectangle: CGRect) -> CGFloat {
        let inch = (pow(rectangle.width, 2) + pow(rectangle.height, 2)).squareRoot()
        return inch*percent / 100
        
    }
}
