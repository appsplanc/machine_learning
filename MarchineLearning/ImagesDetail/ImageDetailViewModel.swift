//
//  ImageDetailViewModel.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 6/4/21.
//

import UIKit
import Vision

protocol ImageDetailCoordinatorDelegate: class {
    func imageDetailCancelButtonTapped()
}

protocol ImageDetailViewDelegate: class {
    func showImageView(image: UIImage?)
    func hideProgressView()
}

class ImageDetailViewModel {
    private var classifier: VisionObservation?
    weak var coordinatorDelegate: ImageDetailCoordinatorDelegate?
    weak var viewDelegate: ImageDetailViewDelegate?
    let image: Image
    
    init(image: Image) {
        self.image = image
        do {
            let coreModel = try VNCoreMLModel(for: YOLOv3(configuration: MLModelConfiguration()).model)
            self.classifier = VisionObservation(coreModel: coreModel)
        } catch {
            fatalError("ML model: \(error)")
        }
    }
    
    func updateDetections() {
        guard let image = self.image.imageUI else { return }
        classifier?.classify(image) { [weak self] detections in
            guard let newImage = Util.drawDetectionsOnPreview(image: image, detections: detections) else { return }
            DispatchQueue.main.async {
                self?.viewDelegate?.showImageView(image: newImage)
                self?.viewDelegate?.hideProgressView()
            }
        }
    }
    
    func back() {
        coordinatorDelegate?.imageDetailCancelButtonTapped()
    }
    
}
