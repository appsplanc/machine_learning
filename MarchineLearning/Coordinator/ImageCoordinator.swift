//
//  ImageCoordinator.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 5/4/21.
//

import UIKit

class ImageCoordinator: Coordinator {
    let presenter: UINavigationController
    let imageDataManager: ImageDataManager
    var imageViewModel: ImageViewModel?

    init(presenter: UINavigationController, imageDataManager: ImageDataManager) {
        self.presenter = presenter
        self.imageDataManager = imageDataManager
    }
    
    
    override func start() {
        let imageViewModel = ImageViewModel(imageDataManager: imageDataManager)
        let imageViewController = ImageViewController(viewModel: imageViewModel)
        imageViewController.title = NSLocalizedString("Images", comment: "")
        imageViewModel.viewDelegate = imageViewController
        imageViewModel.coordinatorDelegate = self
        self.imageViewModel = imageViewModel
        presenter.pushViewController(imageViewController, animated: true)
    }
    
    override func finish() {}
}


extension ImageCoordinator: ImageCoordinatorDelegate {
    func didSelect(image: Image) {
        let imageDetailViewModel = ImageDetailViewModel(image: image)
        imageDetailViewModel.coordinatorDelegate = self
        let imageDetailViewController = ImageDetailViewController(viewModel: imageDetailViewModel)
        imageDetailViewController.title = NSLocalizedString("Detalle", comment: "")
        imageDetailViewModel.viewDelegate = imageDetailViewController
        presenter.pushViewController(imageDetailViewController, animated: true)
    }
    
}


extension ImageCoordinator: ImageDetailCoordinatorDelegate {
    func imageDetailCancelButtonTapped() {
        presenter.popViewController(animated: true)
    }
    
    
}
