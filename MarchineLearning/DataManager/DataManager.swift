//
//  DataManager.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 5/4/21.
//

import Foundation


class DataManager {
    let localDataManager: LocalDataManager
    
    init(localDataManager: LocalDataManager) {
        self.localDataManager = localDataManager
    }
}


extension DataManager: ImageDataManager {
    func fetchAllImage(completion: @escaping ([Image]?) -> ()) {
        localDataManager.fetchAllImage(completion: completion)
    }
}



extension DataManager: ImageSearchDataManager {
    func fetchSearchAllImage(completion: @escaping ([Image]?) -> ()) {
        localDataManager.fetchSearchAllImage(completion: completion)
    }
}
