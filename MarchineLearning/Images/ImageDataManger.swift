//
//  ImageDataManger.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 5/4/21.
//

import Foundation

protocol ImageDataManager {
    func fetchAllImage(completion: @escaping ([Image]?) -> ())
}
