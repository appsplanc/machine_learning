//
//  VisionObservation.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 10/4/21.
//

import UIKit
import Vision


class VisionObservation {
    private let model: VNCoreMLModel
    private var completion: ([VNRecognizedObjectObservation]) -> Void = { _  in }
    
    private lazy var requests: [VNCoreMLRequest] = {
        
        let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
            guard let results = request.results else {
              print(error!.localizedDescription)
              return
            }
            let detections = results as! [VNRecognizedObjectObservation]
            
            self?.completion(detections)
        })
        
        request.imageCropAndScaleOption = .scaleFit
        return [request]
        
    }()
    
    init?(coreModel: VNCoreMLModel) {
        self.model = coreModel
    }
    
    func classify(_ image: UIImage, completion: @escaping ([VNRecognizedObjectObservation]) -> Void) {
        
        self.completion = completion
        
        DispatchQueue.global().async {
            
            guard let cgImage = image.cgImage else {
                return
            }
            
            let handler = VNImageRequestHandler(cgImage: cgImage, options: [:])
            
            do {
                try handler.perform(self.requests)
            } catch {
                print(error.localizedDescription)
            }
            
        }
        
    }
    
    
}
