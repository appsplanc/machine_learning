//
//  LocalDataManagerImpl.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 5/4/21.
//

import UIKit


class LocalDataManagerImpl: LocalDataManager {
    func fetchSearchAllImage(completion: @escaping ([Image]?) -> ()) {
        var images: [Image] = []
        for index in 1...100 {
            let image = Image(imageUI: UIImage(named: "\(index)"))
            images.append(image)
        }
        completion(images)
    }
    
    func fetchAllImage(completion: @escaping ([Image]?) -> ()) {
        var images: [Image] = []
        for index in 1...100 {
            let image = Image(imageUI: UIImage(named: "\(index)"))
            images.append(image)
        }
        completion(images)
    }
    
    
}
