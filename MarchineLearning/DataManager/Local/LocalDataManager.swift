//
//  LocalDataManager.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 5/4/21.
//

import Foundation

protocol LocalDataManager {
    func fetchAllImage(completion: @escaping ([Image]?) -> ())
    func fetchSearchAllImage(completion: @escaping ([Image]?) -> ())
}
