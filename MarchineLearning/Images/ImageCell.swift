//
//  ImageCell.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 5/4/21.
//

import UIKit

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var imageV: UIImageView!
 
    
    var viewModel: ImageCellViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }
            imageV?.image =  viewModel.image.imageUI
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        imageV.layer.cornerRadius = imageV.frame.size.width / 2
        imageV.clipsToBounds = true
    }
}
