//
//  ImageSearchDataManager.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 9/4/21.
//

import Foundation

protocol ImageSearchDataManager {
    func fetchSearchAllImage(completion: @escaping ([Image]?) -> ())
}

