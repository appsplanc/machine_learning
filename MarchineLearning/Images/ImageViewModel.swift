//
//  ImageViewModel.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 5/4/21.
//

import UIKit


protocol ImageCoordinatorDelegate: class {
    func didSelect(image: Image)
}

protocol ImageViewDelegate: class {
    func imageFetched()
}

class ImageViewModel {
    weak var coordinatorDelegate: ImageCoordinatorDelegate?
    weak var viewDelegate: ImageViewDelegate?
    let imageDataManager: ImageDataManager
    var imageViewModels: [ImageCellViewModel] = []
    
    init(imageDataManager: ImageDataManager) {
        self.imageDataManager = imageDataManager
    }
    
    func viewWasLoaded() {
        imageDataManager.fetchAllImage { images in
            guard let images = images else {return}
            self.imageViewModels = images.map({ ImageCellViewModel(image: $0 ) })
            self.viewDelegate?.imageFetched()
        }
    }
    
    func numberOfSections() -> Int {
        return 1
    }

    func numberOfRows(in section: Int) -> Int {
        return imageViewModels.count
    }

    func viewModel(at indexPath: IndexPath) -> ImageCellViewModel? {
        guard indexPath.row < imageViewModels.count else { return nil }
        return imageViewModels[indexPath.row]
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        coordinatorDelegate?.didSelect(image: self.imageViewModels[indexPath.row].image)
    }
}
