//
//  AppCoordinator.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 5/4/21.
//

import UIKit


class AppCoordinator: Coordinator {
    
    lazy var localDataManager: LocalDataManager = {
        let localDataManager = LocalDataManagerImpl()
        return localDataManager
    }()
    
    lazy var dataManager: DataManager = {
        let dataManager = DataManager(localDataManager: self.localDataManager)
        return dataManager
    }()
    
    
    let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    override func start() {
        let tabBarController = UITabBarController()

        let imageNavigationController = UINavigationController()
        let imageCoordinator = ImageCoordinator(presenter: imageNavigationController,
                                                  imageDataManager: dataManager)
        addChildCoordinator(imageCoordinator)
        imageCoordinator.start()
        
        
        let imageSearchNavigationController = UINavigationController()
        let imageSearchCoordinator = ImageSearchCoordinator(presenter: imageSearchNavigationController, imageSearchDataManager: dataManager)
        addChildCoordinator(imageSearchCoordinator)
        imageSearchCoordinator.start()

      
        tabBarController.tabBar.tintColor = .black

        tabBarController.viewControllers = [imageNavigationController, imageSearchNavigationController]
        tabBarController.tabBar.items?.first?.image = UIImage(systemName: "list.dash")
        tabBarController.tabBar.items?[1].image = UIImage(systemName: "magnifyingglass")
        
        window.rootViewController = tabBarController
        window.makeKeyAndVisible()
    }
    
    override func finish() {}
}
