//
//  ImageSearchCellViewModel.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 9/4/21.
//

import Foundation


class ImageSearchCellViewModel {
    let image: Image
    
    init(image: Image) {
        self.image = image
    }
}
