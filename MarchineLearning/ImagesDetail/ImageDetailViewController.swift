//
//  ImageDetailViewController.swift
//  MarchineLearning
//
//  Created by Félix Luján Albarrán on 6/4/21.
//

import UIKit

class ImageDetailViewController: UIViewController {
    let imageView: UIImageView = {
        let uiImage = UIImageView()
        uiImage.translatesAutoresizingMaskIntoConstraints = false
        uiImage.contentMode = .scaleAspectFit
        return uiImage
    }()
    
    lazy var backButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(NSLocalizedString("Volver Atrás", comment: ""), for: .normal)
        button.backgroundColor = .purple
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 5.0
        button.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var progress: UIActivityIndicatorView = {
        let progress = UIActivityIndicatorView(style: .large)
        progress.center = view.center
        progress.translatesAutoresizingMaskIntoConstraints = false
        progress.color = .purple
        progress.startAnimating()
        progress.isHidden = false
        return progress
    }()
    
    let viewModel: ImageDetailViewModel
    
    init(viewModel: ImageDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    override func loadView() {
        view = UIView()
        view.backgroundColor = .white
        navigationItem.setHidesBackButton(true, animated: true);

        imageView.image = viewModel.image.imageUI
        view.addSubview(imageView)
        
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: 380),
            imageView.topAnchor.constraint(equalTo: view.topAnchor),
            imageView.leftAnchor.constraint(equalTo: view.leftAnchor),
            imageView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])
        
        view.addSubview(backButton)
        NSLayoutConstraint.activate([
            backButton.heightAnchor.constraint(equalToConstant: 50),
            backButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            backButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            backButton.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 10)
        ])
        
        imageView.addSubview(progress)
        NSLayoutConstraint.activate([
            progress.centerYAnchor.constraint(equalTo: imageView.centerYAnchor, constant: 0),
            progress.centerXAnchor.constraint(equalTo: imageView.centerXAnchor, constant: 0),
           
        ])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel.updateDetections()
    }
    
    @objc func backButtonTapped() {
        viewModel.back()
    }
}


extension ImageDetailViewController: ImageDetailViewDelegate {
    func showImageView(image: UIImage?) {
        guard let image = image else { return }
        imageView.image = image
    }
    
    func hideProgressView() {
        progress.isHidden = true
    }
    
}
